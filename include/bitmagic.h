#pragma once

#include <cstddef>
#include <cstdint>
#include <limits>

namespace bitmagic {

template<typename IntT, size_t IntTSize = sizeof(IntT)>
constexpr size_t count_set_bits(const IntT& n) {
    size_t set_bits = 0;
    size_t n_max = std::numeric_limits<IntT>().max();
    for (size_t i = 1; i < n_max; i <<= 1) {
        set_bits += (n & i) > 0;
    }
    return set_bits;
}

}
#if !defined(BITMAGIC_NO_ALIAS)
// define BITMAGIC_NO_ALIAS to turn this off
namespace bm = bitmagic;
#endif
