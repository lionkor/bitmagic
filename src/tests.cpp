#include <bitmagic.h>
#include <iostream>

#define tests_init()     \
    size_t __failed = 0; \
    size_t __good = 0
#define expect(x)                                  \
    do {                                           \
        bool __eval = (x);                         \
        if (!__eval) {                             \
            std::cout << "failed: " << #x << "\n"; \
            ++__failed;                            \
        } else {                                   \
            ++__good;                              \
        }                                          \
    } while (false)
#define tests_failed() __failed
#define tests_total() __failed + __good
#define tests_good() __good

struct uint4_t {
    uint8_t n : 4;
};

int main() {
    tests_init();
    std::cout << "testing count_set_bits\n";
    // Testing all possible bit counts, from 0 to 64 (8+8+8+8).
    // The order of bits does not matter as this is a bitwise operation.
    expect(bm::count_set_bits(0) == 0);
    expect(bm::count_set_bits(0b1) == 1);
    expect(bm::count_set_bits(0b11) == 2);
    expect(bm::count_set_bits(0b111) == 3);
    expect(bm::count_set_bits(0b1111) == 4);
    expect(bm::count_set_bits(0b11111) == 5);
    expect(bm::count_set_bits(0b111111) == 6);
    expect(bm::count_set_bits(0b1111111) == 7);
    expect(bm::count_set_bits(0b11111111) == 8);
    expect(bm::count_set_bits(0b111111111) == 8 + 1);
    expect(bm::count_set_bits(0b1111111111) == 8 + 2);
    expect(bm::count_set_bits(0b11111111111) == 8 + 3);
    expect(bm::count_set_bits(0b111111111111) == 8 + 4);
    expect(bm::count_set_bits(0b1111111111111) == 8 + 5);
    expect(bm::count_set_bits(0b11111111111111) == 8 + 6);
    expect(bm::count_set_bits(0b111111111111111) == 8 + 7);
    expect(bm::count_set_bits(0b1111111111111111) == 8 + 8);
    expect(bm::count_set_bits(0b11111111111111111) == 8 + 8 + 1);
    expect(bm::count_set_bits(0b111111111111111111) == 8 + 8 + 2);
    expect(bm::count_set_bits(0b1111111111111111111) == 8 + 8 + 3);
    expect(bm::count_set_bits(0b11111111111111111111) == 8 + 8 + 4);
    expect(bm::count_set_bits(0b111111111111111111111) == 8 + 8 + 5);
    expect(bm::count_set_bits(0b1111111111111111111111) == 8 + 8 + 6);
    expect(bm::count_set_bits(0b11111111111111111111111) == 8 + 8 + 7);
    expect(bm::count_set_bits(0b111111111111111111111111) == 8 + 8 + 8);
    expect(bm::count_set_bits(0b1111111111111111111111111) == 8 + 8 + 8 + 1);
    expect(bm::count_set_bits(0b11111111111111111111111111) == 8 + 8 + 8 + 2);
    expect(bm::count_set_bits(0b111111111111111111111111111) == 8 + 8 + 8 + 3);
    expect(bm::count_set_bits(0b1111111111111111111111111111) == 8 + 8 + 8 + 4);
    expect(bm::count_set_bits(0b11111111111111111111111111111) == 8 + 8 + 8 + 5);
    expect(bm::count_set_bits(0b111111111111111111111111111111) == 8 + 8 + 8 + 6);
    expect(bm::count_set_bits(0b1111111111111111111111111111111) == 8 + 8 + 8 + 7);
    expect(bm::count_set_bits(0b11111111111111111111111111111111) == 8 + 8 + 8 + 8);

    

    // END
    std::cout << "passed " << tests_good() << "/" << tests_total() << "\n";
    return tests_failed();
}
