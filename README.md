# bitmagic

Header-only bitwise operation helpers. Just add [include](./include) to your include directories and include [bitmagic.h](./include/bitmagic.h).

All functions are available in the `bitmagic` namespace, and under the alias namespace `bm`. This alias can be turned off with `#define BITMAGIC_NO_ALIAS`, if `bm` conflicts with another library.

Tests are included and can be built with cmake from the included `CMakeLists.txt`. These tests cover all branches. 

Note that, unless a good reason exists, [std::bitset](https://en.cppreference.com/w/cpp/utility/bitset) should be used instead.

Included:

- `size_t bm::count_set_bits(const IntT& n)` - counts all bits in the integer which are '1'

